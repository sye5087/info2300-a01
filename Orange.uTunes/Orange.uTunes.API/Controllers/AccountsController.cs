﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Orange.uTunes.API.Repositories;
using Orange.uTunes.API.Entities;
using Orange.uTunes.API.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;

namespace Orange.uTunes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        [HttpGet]
        [Route("logout", Name = "Logout")]
        public IActionResult Logout()
        {
            // end session/expire token

            AuthenticationDTO authenticationDTO = new AuthenticationDTO();
            authenticationDTO.Message = "You are no longer authorized.";

            return Ok(authenticationDTO);
        }

        // POST api/songs
        [HttpPost]
        [Route("login", Name = "Login")]
        public IActionResult Login([FromBody] LoginDTO loginDTO)
        {
            // some actual authentication process
            // and creation of a session token upon success

            // ...but for now we can just hardcode
            // a single set of authentication details

            bool result = (
                loginDTO.Username == "username" &&
                loginDTO.Password == "password"
                );

            AuthenticationDTO authenticationDTO = new AuthenticationDTO();

            if (!result)
            {
                authenticationDTO.Message = "You are not authorized.";
                return Ok(StatusCode(200, authenticationDTO));
            }

            authenticationDTO.Message = "You are authorized.";
            authenticationDTO.Token = "ThisIsDefinitelyARealTokenForAnAuthenticatedUser123!";

            return Ok(StatusCode(200, authenticationDTO));
        }
    }
}
