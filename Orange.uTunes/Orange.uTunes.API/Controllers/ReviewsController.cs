﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Orange.uTunes.API.Repositories;
using Orange.uTunes.API.Entities;
using Orange.uTunes.API.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;

namespace Orange.uTunes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private IReviewRepository _reviewRepository;

        public ReviewsController(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        [HttpGet]
        public IActionResult GetAllReviews()
        {
            var allReviews = _reviewRepository.GetAll().ToList();

            var allReviewsDto = allReviews.Select(x => Mapper.Map<ReviewDTO>(x)).ToArray();

            return Ok(allReviewsDto);
        }

        [HttpGet]
        [Route("{id}", Name = "GetSingleReview")]
        public IActionResult GetSingleReview(Guid id)
        {
            Review reviewFromRepo = _reviewRepository.GetSingle(id);

            if (reviewFromRepo == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ReviewDTO>(reviewFromRepo));
        }

        // POST api/reviews
        [HttpPost]
        public IActionResult AddReview([FromBody] ReviewCreateDTO reviewCreateDto)
        {
            Review toAdd = Mapper.Map<Review>(reviewCreateDto);

            _reviewRepository.Add(toAdd);

            bool result = _reviewRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            //return Ok(Mapper.Map<CustomerDto>(toAdd));
            return CreatedAtRoute("GetSingleReview", new { id = toAdd.Id }, Mapper.Map<ReviewDTO>(toAdd));
        }

        // PUT api/reviews/5
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateReview(Guid id, [FromBody] ReviewUpdateDTO updateDto)
        {
            var existingReview = _reviewRepository.GetSingle(id);

            if (existingReview == null)
            {
                return NotFound();
            }

            Mapper.Map(updateDto, existingReview);

            _reviewRepository.Update(existingReview);

            bool result = _reviewRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<ReviewDTO>(existingReview));
        }

        [HttpPatch]
        [Route("{id}")]
        public IActionResult PartiallyUpdate(Guid id, [FromBody] JsonPatchDocument<ReviewUpdateDTO> reviewPatchDoc)
        {
            if (reviewPatchDoc == null)
            {
                return BadRequest();
            }

            var existingReview = _reviewRepository.GetSingle(id);

            if (existingReview == null)
            {
                return NotFound();
            }

            var customerToPatch = Mapper.Map<ReviewUpdateDTO>(existingReview);
            reviewPatchDoc.ApplyTo(customerToPatch);

            Mapper.Map(customerToPatch, existingReview);

            _reviewRepository.Update(existingReview);

            bool result = _reviewRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<ReviewDTO>(existingReview));
        }

        // DELETE api/reviews/5
        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            var existingReview = _reviewRepository.GetSingle(id);

            if (existingReview == null)
            {
                return NotFound();
            }

            _reviewRepository.Delete(id);

            bool result = _reviewRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return NoContent();
        }
    }
}
