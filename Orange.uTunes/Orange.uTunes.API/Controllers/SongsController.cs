﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Orange.uTunes.API.Repositories;
using Orange.uTunes.API.Entities;
using Orange.uTunes.API.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;

namespace Orange.uTunes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongsController : ControllerBase
    {
        private ISongRepository _songRepository;

        public SongsController(ISongRepository songRepository)
        {
            _songRepository = songRepository;
        }

        [HttpGet]
        public IActionResult GetAllSongs()
        {
            var allSongs = _songRepository.GetAll().ToList();

            var allSongsDto = allSongs.Select(x => Mapper.Map<SongDTO>(x)).ToArray();

            return Ok(allSongsDto);
        }

        [HttpGet]
        [Route("{id}", Name = "GetSingleSong")]
        public IActionResult GetSingleSong(Guid id)
        {
            Song songFromRepo = _songRepository.GetSingle(id);

            if (songFromRepo == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<SongDTO>(songFromRepo));
        }

        // POST api/songs
        [HttpPost]
        public IActionResult AddSong([FromBody] SongCreateDTO songCreateDto)
        {
            Song toAdd = Mapper.Map<Song>(songCreateDto);

            _songRepository.Add(toAdd);

            bool result = _songRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            //return Ok(Mapper.Map<CustomerDto>(toAdd));
            return CreatedAtRoute("GetSingleSong", new { id = toAdd.Id }, Mapper.Map<SongDTO>(toAdd));
        }

        // PUT api/songs/5
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateSong(Guid id, [FromBody] SongUpdateDTO updateDto)
        {
            var existingSong = _songRepository.GetSingle(id);

            if (existingSong == null)
            {
                return NotFound();
            }

            Mapper.Map(updateDto, existingSong);

            _songRepository.Update(existingSong);

            bool result = _songRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<SongDTO>(existingSong));
        }

        [HttpPatch]
        [Route("{id}")]
        public IActionResult PartiallyUpdate(Guid id, [FromBody] JsonPatchDocument<SongUpdateDTO> songPatchDoc)
        {
            if (songPatchDoc == null)
            {
                return BadRequest();
            }

            var existingSong = _songRepository.GetSingle(id);

            if (existingSong == null)
            {
                return NotFound();
            }

            var customerToPatch = Mapper.Map<SongUpdateDTO>(existingSong);
            songPatchDoc.ApplyTo(customerToPatch);

            Mapper.Map(customerToPatch, existingSong);

            _songRepository.Update(existingSong);

            bool result = _songRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return Ok(Mapper.Map<SongDTO>(existingSong));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            var existingSong = _songRepository.GetSingle(id);

            if (existingSong == null)
            {
                return NotFound();
            }

            _songRepository.Delete(id);

            bool result = _songRepository.Save();

            if (!result)
            {
                return new StatusCodeResult(500);
            }

            return NoContent();
        }
    }
}
