﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Orange.uTunes.API;

namespace Orange.uTunes.API.Middlewares
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly MyConfiguration _myConfig;

        public CustomMiddleware(RequestDelegate next, IOptions<MyConfiguration> myconfig)
        {
            _next = next;
            _myConfig = myconfig.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            Debug.WriteLine($"---> Request for {httpContext.Request.Path} from {_myConfig.username}");
            //call the next middleware delegate in the pipeline
            await _next.Invoke(httpContext);
        }
    }
}
