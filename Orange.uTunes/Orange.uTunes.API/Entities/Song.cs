﻿using System;
namespace Orange.uTunes.API.Entities
{
    public class Song
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ArtistName { get; set; }
    }
}
