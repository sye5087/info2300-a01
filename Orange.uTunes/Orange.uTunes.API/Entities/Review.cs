﻿using System;
namespace Orange.uTunes.API.Entities
{
    public class Review
    {
        public Guid Id { get; set; }
        public Guid SongId { get; set; }
        public string AuthorName { get; set; }
        public string Body { get; set; }
        public int Rating { get; set; }

        public Song Song { get; set; }
    }
}
