﻿using System;
namespace Orange.uTunes.API.Entities
{
    public class Account
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
