﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Orange.uTunes.API.Entities
{
    public class OrangeuTunesContext : DbContext
    { 
        //use sqlite
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Orange.uTunes.db");
        }

        public DbSet<Song> Songs { get; set; }
        public DbSet<Review> Reviews { get; set; }

    }
}
