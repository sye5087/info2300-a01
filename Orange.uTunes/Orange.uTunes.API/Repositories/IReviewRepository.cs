﻿using System;
using System.Linq;
using Orange.uTunes.API.Entities;

namespace Orange.uTunes.API.Repositories
{
    public interface IReviewRepository
    {
        void Add(Review item);
        void Delete(Guid id);
        IQueryable<Review> GetAll();
        Review GetSingle(Guid id);
        bool Save();
        void Update(Review item);
    }
}