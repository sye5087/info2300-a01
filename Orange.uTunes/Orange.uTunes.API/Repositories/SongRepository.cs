﻿using System;
using System.Linq;
using Orange.uTunes.API.Entities;

namespace Orange.uTunes.API.Repositories
{
    public class SongRepository : ISongRepository
    {
        private OrangeuTunesContext _context;

        public SongRepository(OrangeuTunesContext context)
        {
            _context = context;
        }

        public IQueryable<Song> GetAll()
        {
            return _context.Songs;
        }

        public Song GetSingle(Guid id)
        {
            return _context.Songs.FirstOrDefault(s => s.Id == id);
        }

        public void Add(Song item)
        {
            _context.Add(item);
        }

        public void Delete(Guid id)
        {
            Song Song = GetSingle(id);
            _context.Songs.Remove(Song);
        }


        public void Update(Song item)
        {
            _context.Songs.Update(item);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}
