﻿using System;
using System.Linq;
using Orange.uTunes.API.Entities;

namespace Orange.uTunes.API.Repositories
{
    public interface ISongRepository
    {
        void Add(Song item);
        void Delete(Guid id);
        IQueryable<Song> GetAll();
        Song GetSingle(Guid id);
        bool Save();
        void Update(Song item);
    }
}