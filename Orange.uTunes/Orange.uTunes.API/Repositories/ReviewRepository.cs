﻿using System;
using System.Linq;
using Orange.uTunes.API.Entities;

namespace Orange.uTunes.API.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private OrangeuTunesContext _context;

        public ReviewRepository(OrangeuTunesContext context)
        {
            _context = context;
        }

        public IQueryable<Review> GetAll()
        {
            return _context.Reviews;
        }

        public Review GetSingle(Guid id)
        {
            return _context.Reviews.FirstOrDefault(s => s.Id == id);
        }

        public void Add(Review item)
        {
            _context.Add(item);
        }

        public void Delete(Guid id)
        {
            Review Review = GetSingle(id);
            _context.Reviews.Remove(Review);
        }

        public void Update(Review item)
        {
            _context.Reviews.Update(item);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}
