﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Orange.uTunes.API.Entities;
using Orange.uTunes.API.DTOs;
using Orange.uTunes.API.Middlewares;
using Orange.uTunes.API.Repositories;

namespace Orange.uTunes.API
{
    // This would normally be in a different file. Simplified.
    public class MyConfiguration
    {
        public string username { get; set; }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            using (var client = new OrangeuTunesContext())
            {
                client.Database.EnsureCreated();
            }
            Debug.WriteLine($"---> From Config: {Configuration["firstname"]}");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddOptions();
            services.Configure<MyConfiguration>(Configuration);
            services.AddEntityFrameworkSqlite().AddDbContext<OrangeuTunesContext>();
            services.AddScoped<ISongRepository, SongRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            AutoMapper.Mapper.Initialize(mapper =>
            {
                mapper.CreateMap<Song, SongDTO>().ReverseMap();
                mapper.CreateMap<Song, SongCreateDTO>().ReverseMap();
                mapper.CreateMap<Song, SongUpdateDTO>().ReverseMap();
                mapper.CreateMap<Review, ReviewDTO>().ReverseMap();
                mapper.CreateMap<Review, ReviewCreateDTO>().ReverseMap();
                mapper.CreateMap<Review, ReviewUpdateDTO>().ReverseMap();
            });

            app.UseMvc();

            app.UseCustomMiddleware();
        }
    }
}
