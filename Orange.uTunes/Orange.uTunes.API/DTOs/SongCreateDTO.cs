﻿using System;
namespace Orange.uTunes.API.DTOs
{
    public class SongCreateDTO
    {
        public string Name { get; set; }
        public string ArtistName { get; set; }
    }
}
