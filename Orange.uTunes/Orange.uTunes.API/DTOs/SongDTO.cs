﻿using System;
namespace Orange.uTunes.API.DTOs
{
    public class SongDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ArtistName { get; set; }
    }
}
