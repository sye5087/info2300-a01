﻿using System;
namespace Orange.uTunes.API.DTOs
{
    public class ReviewDTO
    {
        public Guid Id { get; set; }
        public Guid SongId { get; set; }
        public string AuthorName { get; set; }
        public string Body { get; set; }
        public int Rating { get; set; }
    }
}
