﻿using System;
namespace Orange.uTunes.API.DTOs
{
    public class AuthenticationDTO
    {
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
