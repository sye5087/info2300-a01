# Orange uTunes API

To get started on development, open the included .sln file within the Orange.uTunes directory using Visual Studio 2019.

Alongside this README is a LICENSE file describing the permissions, conditions, and limitations of use of this code. I've chosen an MIT license as it is simple and permissive, and requires only preservation of copyright and license notices, enabling others to freely use my contributions, without liability.
